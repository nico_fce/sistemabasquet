La intensión es hacer herencia con las siguientes clases, para despues llamarlas en la clase program:

Clase Persona
Clase Jugador: Persona
Clase Cuerpo_Tecnico:Persona

Para despues instanciar una clase lista en program, para almacenar los inputs del array en el constructor correspondente.

También Hay herencia en la Clase Lista

Clase Lista
Clase ListaJugador : Lista
Clase Lista_Cuerpo_Tecnico : Lista

La idea es poder crear la lista y poder ir haciendo override del método Ingresar() en cuanto corresponda