﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaBasquet
{
    class Persona
    {
        //La idea que esta sea la Clase Padre para tener un objeto con un constructor para almacenar la
        //lista que se cree en la otra Clase "Lista".

        private long dni = 0;
        private string nombre = "";
        private string apellido = "";

        public long Dni
        {
            set { dni = value; }
            get { return dni; }
        }
        public string Nombre
        {
            set { nombre = value; }
            get { return nombre; }
        }
        public string Apellido
        {
            set { apellido = value; }
            get { return apellido; }
        }

        public Persona(long dni, string nombre, string apellido)
        {
            this.Dni = dni;
            this.Nombre = nombre;
            this.Apellido = apellido;
        }

        public virtual string Detallar()
        {
            return "Mi DNI es: " + Dni ;
        }

    }
    class Jugador : Persona //La intención es que herede una parte del constructor Base.
    {
        private string posicion = "";
        private int dorsal = 0;

        public string Posicion
        {
            set { posicion = value; }
            get { return posicion; }
        }

        public int Dorsal
        {
            set { dorsal = value; }
            get { return dorsal; }
        }
        // Me gustaría que puesto sea el enumerador "EnumPosicion"
        public Jugador(long dni, string nombre, string apellido, string posicion, int dorsal):base(dni,nombre,apellido) 
        {
            this.Posicion = posicion;
            this.Dorsal = dorsal;
        }
    
    }
    class CuerpoTecnico : Persona //La intención es que herede una parte del constructor Base.
    {
        private string puesto = "";
        public string Puesto
        {
            set { puesto = value; }
            get { return puesto; }
        }

        // Me gustaría que puesto sea el enumerador "EnumPuesto"
        public CuerpoTecnico(long dni, string nombre, string apellido, string puesto):base(dni,nombre,apellido)
        {
            this.Puesto = puesto;
        }
       
    }

    public enum EnumPosicion 
    {
        Base = 1, Escolta, Alero, Ala_Pivot, Pivot
    }

    public enum EnumPuesto
    {
        Entrenador = 1, Doctor, Kinesiologo, Psicologo, AyudanteCampo
    }


}