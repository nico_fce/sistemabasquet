﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaBasquet
{
    class Lista
    {
        //La idea es crear una clase Lista Padre, la cual tiene dos hijas que son "ListaJugador" y
        // y "ListaCT (Cuerpo Técnico)" y que se vaya modificando, dependiendo en que objeto de la
        // clase Persona que quiera almacenar.
        private int ultNum = 0;
        private int ultNumArray = 0;
        public Persona[] listaPersona;
        public Jugador[] listaJugador;
        //public CuerpoTecnico[] listaCT;
        public Validacion val = new Validacion();//Instancia de Clase
        public int UltNum
        {
            set { ultNum = value;  }
            get { return ultNum; }            
        }

        public int UltNumArray
        {
            set { ultNumArray = value; }
            get { return ultNumArray; }
        }


        public Lista(int cantidad)      
        {
            int persona = 0;
            if (cantidad >= 1)
            {
                listaPersona = new Persona[cantidad];
                while (persona <= listaPersona.GetUpperBound(0))
                {
                    listaPersona[persona] = new Persona(0, "", "");
                    persona++;
                }
            }        
        }

        //Método para verificar lugar en el array[].
        public int LugarEnArray(int lugar) //LISTA A REALIZAR POR EL ALUMNO
        {
            int espacio = listaPersona.GetUpperBound(0); //Da 4 (el ultimo entre 0 y 4). Array[5]
            if (lugar <= espacio) //Si el lugar < a 4, devuelvo lugar en el array[].
            {
                return lugar;
            }
            else
            {
                return -1;
            }

        }

        public virtual void Ingresar()                
        {
            int lugar = 0;
            long a = 0; //DNI
            string b = ""; //Nombre
            string c = ""; //Apellido

            lugar = LugarEnArray(0);

            UltNumArray++; //Aumento un valor para un futuro ingreso.
            if (lugar == -1)
            {
                Console.WriteLine("No hay lugar");
            }
            else
            {
                a = val.PedirLong("Ingresar Documento", 1000000, 99999999);
                b = val.PedirStrNoVac("Ingresar Nombre");
                c = val.PedirStrNoVac("Ingresar Apellido");

                UltNum++; //Aumento número de reserva

                listaPersona[lugar] = new Persona(a,b,c); //Guardo valores en el array.
                Console.WriteLine("Se ingresó DNI: " + a);
            }
        }

    }

    class ListaJugador : Lista
    {
        public ListaJugador(int cantidad) : base(cantidad)
        {
            int jugador = 0;
            if (cantidad >= 1)
            {
                listaJugador = new Jugador[cantidad];
                while (jugador <= listaJugador.GetUpperBound(0))
                {
                    listaJugador[jugador] = new Jugador(0, "", "","",0);
                }
            }

        }
        public override void Ingresar()
        {
            int lugar = 0;
            long a = 0; //DNI
            string b = ""; //Nombre
            string c = ""; //Apellido
            int d = 0; //Legajo
            string e = ""; //Posicion
            //int f = 0; //Dorsal

            lugar = LugarEnArray(0);

            UltNumArray++; //Aumento un valor para un futuro ingreso.
            if (lugar == -1)
            {
                Console.WriteLine("No hay lugar");
            }
            else
            {
                a = val.PedirLong("Ingresar Documento", 1000000, 99999999);
                b = val.PedirStrNoVac("Ingresar Nombre");
                c = val.PedirStrNoVac("Ingresar Apellido");                
                e = val.PedirStrNoVac("Ingresar puesto");
                Console.WriteLine("Ingrese dorsal:\n");
                d = Convert.ToInt32(Console.ReadLine());

                UltNum++; //Aumento número de reserva

                listaPersona[lugar] = new Jugador(a, b, c, e, d ); //Guardo valores en el array.
                Console.WriteLine("Se ingresó DNI: " + a);
            }
        }

    }

}

