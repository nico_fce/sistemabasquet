﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaBasquet
{
    class Program
    {
        static void Main(string[] args)
        {
            const string opcJugador = "A"; //Jugadores
            const string opcCT = "B"; //Cuerpo Técnico
            const string opcEstadisticas = "C"; //Estadísticas
            const string opcSalir = "S"; //Opción Salir
            const int cantReservas = 5; //Tamaño máximo del Array
            string opcion = ""; //Opción seleccionada por el Usuario
 
            Validacion miVal = new Validacion(); //Instancia de Clase
            
            Lista miLista = new Lista(cantReservas);
            //La idea es instanciar la Lista o Lista Heredada que de la Clase Lista en cuando corresponda.
            Console.WriteLine("Sistema Liga de Básquet");

            do
            {
                opcion = miVal.PedirStrNoVac("-----------------------\n\t Menú\n-----------------------\t" +
                "\nSeleccione Módulo: \n" 
                + opcJugador + ": Jugadores\n"
                + opcCT + ": Cuerpo Técnico\n"
                + opcEstadisticas + ": Estadísticas\n"
                + opcSalir + ": Salir");
                switch (opcion)
                {
                    case opcJugador:
                        miLista.Ingresar();
                        break;
                    case opcCT:
                        Console.WriteLine("Construir");
                        //listaR.Finalizar();
                        break;
                    case opcEstadisticas:
                        Console.WriteLine("Construir");
                        //listaR.Modificar();
                        break;
                    case opcSalir:
                        break;
                    default:
                        Console.WriteLine("Opción no válida");
                        break;
                }

            } while (opcion != opcSalir);
        }
    }
}
